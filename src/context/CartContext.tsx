import produce from 'immer'
import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState
} from 'react'

export type Coffee = {
  id: number
  tags: string[]
  name: string
  description: string
  photo: string
  price: number
}

export type CartItemProps = {
  quantity: number
} & Coffee

type CartContextProps = {
  cartItems: CartItemProps[]
  cartQuantity: number
  cartItemsTotal: number
  addCoffeeToCart: (coffee: CartItemProps) => void
  changeCartItemQuantity: (
    cartItemId: number,
    type: 'increase' | 'decrease'
  ) => void
  removeCartItem: (cartItemId: number) => void
  clearCart: () => void
}

type CartContextProviderProps = {
  children: ReactNode
}

export const CartContext = createContext({} as CartContextProps)

const COFFEE_ITEMS_STORAGE_KEY = 'coffeeDelivery:cartItems'

export function CartContextProvider({ children }: CartContextProviderProps) {
  const [cartItems, setCartItems] = useState<CartItemProps[]>(() => {
    const storedCartItems = localStorage.getItem(COFFEE_ITEMS_STORAGE_KEY)
    if (storedCartItems) {
      return JSON.parse(storedCartItems)
    }
    return []
  })

  const cartQuantity = cartItems.length

  const cartItemsTotal = cartItems.reduce((total, cartItems) => {
    return total + cartItems.price * cartItems.quantity
  }, 0)

  function addCoffeeToCart(coffee: CartItemProps) {
    const coffeeAlreadyExistsInCart = cartItems.findIndex(
      (cartItem) => cartItem.id === coffee.id
    )

    const newItemToCart = produce(cartItems, (draft) => {
      if (coffee.quantity > 0) {
        if (coffeeAlreadyExistsInCart < 0) {
          draft.push(coffee)
        } else {
          draft[coffeeAlreadyExistsInCart].quantity += coffee?.quantity
        }
      }
    })

    setCartItems(newItemToCart)
  }

  function changeCartItemQuantity(
    cartItemId: number,
    type: 'increase' | 'decrease'
  ) {
    const changeCartItem = produce(cartItems, (draft) => {
      const coffeeExistsInCart = cartItems.findIndex(
        (cartItem) => cartItem.id === cartItemId
      )

      if (coffeeExistsInCart >= 0) {
        const item = draft[coffeeExistsInCart]
        draft[coffeeExistsInCart].quantity =
          type === 'increase' ? item.quantity + 1 : item.quantity - 1
      }
    })

    setCartItems(changeCartItem)
  }

  function removeCartItem(cartItemId: number) {
    const removeItem = produce(cartItems, (draft) => {
      const itemInCart = cartItems.findIndex((item) => item.id === cartItemId)

      if (itemInCart >= 0) {
        draft.splice(itemInCart, 1)
      }
    })

    setCartItems(removeItem)
  }

  function clearCart() {
    setCartItems([])
  }

  useEffect(() => {
    localStorage.setItem(COFFEE_ITEMS_STORAGE_KEY, JSON.stringify(cartItems))
  }, [cartItems])

  return (
    <CartContext.Provider
      value={{
        cartItems,
        cartQuantity,
        cartItemsTotal,
        clearCart,
        addCoffeeToCart,
        changeCartItemQuantity,
        removeCartItem
      }}
    >
      {children}
    </CartContext.Provider>
  )
}

export function useCart() {
  return useContext(CartContext)
}
