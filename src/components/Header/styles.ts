import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.header`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 10.4rem;

  ${({ theme }) => css`
    background: ${theme.colors.background};
  `};

  nav {
    display: flex;
    gap: 1.2rem;
  }
`
const BaseButton = styled.button`
  display: flex;
  align-items: center;
  padding: 0.8rem;
  position: relative;

  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size14};
    border-radius: ${theme.border.radius};
  `};
`

export const MapButton = styled(BaseButton)`
  ${({ theme }) => css`
    background: ${theme.colors.purpleLight};
    color: ${theme.colors.purpleDark};

    > svg {
      margin-right: 0.4rem;
      color: ${theme.colors.purple};
    }
  `};
`

export const CartButton = styled(BaseButton)`
  ${({ theme }) => css`
    background: ${theme.colors.yellowLight};
    color: ${theme.colors.yellowDark};

    > span {
      position: absolute;
      /* top: calc(-2rem / 2);
      right: calc(-2rem / 2); */
      top: -1rem;
      right: -1rem;
      width: 2rem;
      height: 2rem;
      border-radius: 50%;
      background: ${theme.colors.yellowDark};
      color: ${theme.colors.white};
      font-size: ${theme.font.sizes.size14};

      ${media.lessThan('medium')`
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: ${theme.font.sizes.size12};
      `}
    }
  `};
`
