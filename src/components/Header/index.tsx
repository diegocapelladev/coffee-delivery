import { Link, NavLink } from 'react-router-dom'
import { MapPin, ShoppingCart } from 'phosphor-react'

import logo from '../../assets/coffee-delivery-logo.svg'

import * as S from './styles'
import { Container } from '../Container'
import { useCart } from '../../context/CartContext'

export const Header = () => {
  const { cartQuantity } = useCart()

  return (
    <Container>
      <S.Wrapper>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>

        <nav>
          <S.MapButton color="purple">
            <MapPin size={22} weight="fill" />
            Porto Alegre, RS
          </S.MapButton>

          <NavLink to="/checkout">
            <S.CartButton>
              {cartQuantity >= 1 && <span>{cartQuantity}</span>}
              <ShoppingCart size={22} weight="fill" />
            </S.CartButton>
          </NavLink>
        </nav>
      </S.Wrapper>
    </Container>
  )
}
