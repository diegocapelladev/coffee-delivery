import { HeadingProps } from '.'
import styled, { css, DefaultTheme } from 'styled-components'

export const Modifiers = {
  small: (theme: DefaultTheme) => css`
    font-size: ${theme.font.sizes.size18};
  `,

  medium: (theme: DefaultTheme) => css`
    font-size: ${theme.font.sizes.size32};
  `,

  large: (theme: DefaultTheme) => css`
    font-size: ${theme.font.sizes.size48};
  `
}

export const Wrapper = styled.h2<HeadingProps>`
  ${({ theme, color, size }) => css`
    font-family: 'Baloo 2';
    color: ${theme.colors[color]};

    ${!!size && Modifiers[size](theme)}
  `}
`
