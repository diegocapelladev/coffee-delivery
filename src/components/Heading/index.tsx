import { ReactNode } from 'react'
import * as S from './styles'

export type HeadingProps = {
  children: ReactNode
  color: 'title' | 'yellowDark'
  size: 'small' | 'medium' | 'large'
}

export const Heading = ({ children, color, size }: HeadingProps) => (
  <S.Wrapper size={size} color={color}>
    {children}
  </S.Wrapper>
)
