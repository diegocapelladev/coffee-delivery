import * as S from './styles'

export type IconProps = {
  iconBg: 'yellow' | 'yellowDark' | 'purple' | 'text'
  children: JSX.Element
}

export const Icon = ({ children, iconBg }: IconProps) => (
  <S.Wrapper iconBg={iconBg}>{children}</S.Wrapper>
)
