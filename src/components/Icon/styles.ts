import styled, { css } from 'styled-components'
import { IconProps } from '.'

export const Wrapper = styled.div<Pick<IconProps, 'iconBg'>>`
  ${({ theme, iconBg }) => css`
    width: 3.2rem;
    height: 3.2rem;
    color: ${theme.colors.white};
    background: ${theme.colors[iconBg]};
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
  `}
`
