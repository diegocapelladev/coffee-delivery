import { CartContextProvider } from './context/CartContext'
import { Router } from './Routes'

export function App() {
  return (
    <CartContextProvider>
      <Router />
    </CartContextProvider>
  )
}
