import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 8rem auto;

  ${media.lessThan('medium')`
    margin: 4rem auto;
  `}
`

export const Title = styled.div`
  ${({ theme }) => css`
    > h1 {
      color: ${theme.colors.yellowDark};
      font-size: ${theme.font.sizes.size32};
      font-family: 'Baloo 2';
    }

    > p {
      font-size: ${theme.font.sizes.size20};
      color: ${theme.colors.subtitle};
    }
  `}
`

export const Section = styled.section`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 10.2rem;
  margin-top: 2rem;

  ${media.lessThan('large')`
    display: flex;
    flex-direction: column-reverse;
    justify-content: center;
    align-items: center;
    gap: 4rem;

    > div {
      display: flex;
      justify-content: center;
      max-width: 100%;
    }
  `}
`

export const ShippingCard = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: 3.2rem;

    padding: 4rem;
    width: 100%;

    background:
    linear-gradient(#FAFAFA, #FAFAFA) padding-box,
    linear-gradient(to right, #dbac2c, #8047f8) border-box;
    border-radius: ${theme.border.radius};
    border-bottom-left-radius: 3.6rem;
    border-top-right-radius: 3.6rem;
    border: 1px solid transparent;

    ${media.lessThan('medium')`
      padding: 2rem;
    `}
  `}

  > div {
    display: flex;
    gap: 1.2rem;
    align-items: center;

    > p, strong {
      display: inline-block;
    }
  }


`

export const ShippingImg = styled.div`
  width: 100%;

  ${media.lessThan('medium')`
    img {
      width: 30rem;
    }
  `}
`
