import { Clock, CurrencyDollar, MapPin } from 'phosphor-react'
import { useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import shippingImage from '../../assets/confirmed-order.svg'
import { Container } from '../../components/Container'
import { Icon } from '../../components/Icon'
import { OrderData } from '../Checkout'
import * as S from './styles'

type LocationType = {
  state: OrderData
}

export const Shipping = () => {
  const { state } = useLocation() as unknown as LocationType
  const navigate = useNavigate()

  useEffect(() => {
    if (!state) {
      navigate('/')
    }
  }, [state, navigate])

  return (
    <Container>
      <S.Wrapper>
        <S.Title>
          <h1>Uhu! Pedido confirmado</h1>
          <p>Agora é só aguardar que logo o café chegará até você</p>
        </S.Title>

        <S.Section>
          <S.ShippingCard>
            <div>
              <Icon iconBg="purple">
                <MapPin weight="fill" size={16} />
              </Icon>
              <p>
                Entrega em &nbsp;
                <strong>
                  {state.street}, {state.number}
                </strong>
                <br />
                {state.district} - {state.city}, {state.uf}
              </p>
            </div>

            <div>
              <Icon iconBg="yellow">
                <Clock weight="fill" size={16} />
              </Icon>

              <p>
                Previsão de entrega <br />
                <strong> 20 min - 30 min</strong>
              </p>
            </div>

            <div>
              <Icon iconBg="yellowDark">
                <CurrencyDollar weight="fill" size={16} />
              </Icon>
              <p>
                Pagamento na entrega <br />
                <strong>Cartão de Crédito</strong>
              </p>
            </div>
          </S.ShippingCard>

          <S.ShippingImg>
            <img src={shippingImage} alt="" />
          </S.ShippingImg>
        </S.Section>
      </S.Wrapper>
    </Container>
  )
}
