import { Intro } from './components/Intro'
import { Coffees } from './components/Coffees'

export const Home = () => {
  return (
    <>
      <Intro />

      <Coffees />
    </>
  )
}
