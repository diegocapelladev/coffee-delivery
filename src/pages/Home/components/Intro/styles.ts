import styled, { css } from 'styled-components'
import media from 'styled-media-query'
import heroImageBackground from '../../../../assets/intro-background.png'

export const Wrapper = styled.div`
  width: 100%;
  height: 54.4rem;
  background: url(${heroImageBackground}) no-repeat center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const Hero = styled.div`
  display: flex;

  ${media.lessThan('large')`
    width: 100%;

    > img {
      display: none;
    }
  `}
`

export const HeroText = styled.div`
  margin-right: 5.6rem;

  ${media.lessThan('large')`
    width: 100%;
    margin-right: 0;
  `}
`

export const Text = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: 1.6rem;

    > h1 {
      color: ${theme.colors.title};
      font-size: ${theme.font.sizes.size48};
      font-family: 'Baloo 2';
      font-weight: 800;

      ${media.lessThan('large')`
        font-size: ${theme.font.sizes.size32};
      `}
    }

    > p {
      color: ${theme.colors.subtitle};
      font-size: ${theme.font.sizes.size20};
      font-weight: 400;
      line-height: 1.6;
    }
  `}
`

export const BenefitsItems = styled.div`
  margin-top: 6.6rem;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  row-gap: 2rem;

  ${media.lessThan('medium')`
    grid-template-columns: 1fr;
  `}
`

export const HeroItem = styled.div`
  display: flex;
  align-items: center;

  > p {
    margin-left: 1.2rem;
  }
`
