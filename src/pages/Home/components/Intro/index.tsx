import { Coffee, Package, ShoppingCart, Timer } from 'phosphor-react'

import heroImage from '../../../../assets/intro-img.png'
import { Container } from '../../../../components/Container'
import { Icon } from '../../../../components/Icon'
import * as S from './styles'

export const Intro = () => (
  <S.Wrapper>
    <Container>
      <S.Hero>
        <S.HeroText>
          <S.Text>
            <h1>Encontre o café perfeito para qualquer hora do dia</h1>
            <p>
              Com o Coffee Delivery você recebe seu café onde estiver, a
              qualquer hora
            </p>
          </S.Text>

          <S.BenefitsItems>
            <S.HeroItem>
              <Icon iconBg="yellowDark">
                <ShoppingCart size={16} weight="fill" />
              </Icon>
              <p>Compra simples e segura</p>
            </S.HeroItem>

            <S.HeroItem>
              <Icon iconBg="text">
                <Package size={16} weight="fill" />
              </Icon>
              <p>Embalagem mantém o café intacto</p>
            </S.HeroItem>

            <S.HeroItem>
              <Icon iconBg="yellow">
                <Timer size={16} weight="fill" />
              </Icon>
              <p>Entrega rápida e rastreada</p>
            </S.HeroItem>

            <S.HeroItem>
              <Icon iconBg="purple">
                <Coffee size={16} weight="fill" />
              </Icon>
              <p>O café chega fresquinho até você</p>
            </S.HeroItem>
          </S.BenefitsItems>
        </S.HeroText>

        <img src={heroImage} alt="" />
      </S.Hero>
    </Container>
  </S.Wrapper>
)
