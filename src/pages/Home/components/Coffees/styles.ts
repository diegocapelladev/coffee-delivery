import styled from 'styled-components'
import media from 'styled-media-query'

export const CoffeeList = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  row-gap: 4rem;
  column-gap: 3.2rem;
  margin: 5.4rem auto;

  ${media.lessThan('large')`
    grid-template-columns: repeat(2, 1fr);
  `}

  ${media.lessThan('medium')`
    grid-template-columns: 1fr;
  `}
`
