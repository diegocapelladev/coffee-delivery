import { Container } from '../../../../components/Container'
import { Heading } from '../../../../components/Heading'
import { CoffeeCard } from '../CoffeeCard'
import * as S from './styles'

import { coffees } from '../../../../data/coffes'
import { Coffee } from '../../../../context/CartContext'

export const Coffees = () => {
  return (
    <Container style={{ margin: '3.2rem auto' }}>
      <Heading color="title" size="medium">
        Nossos cafés
      </Heading>
      <S.CoffeeList>
        {coffees.map((coffee: Coffee) => (
          <CoffeeCard key={coffee.id} coffee={coffee} />
        ))}
      </S.CoffeeList>
    </Container>
  )
}
