import { Minus, Plus, ShoppingCart } from 'phosphor-react'
import { useState } from 'react'
import { Coffee, useCart } from '../../../../context/CartContext'
import { formatPrice } from '../../../../utils/formatPrice'
import * as S from './styles'

type CoffeeProps = {
  coffee: Coffee
}

export const CoffeeCard = ({ coffee }: CoffeeProps) => {
  const [quantity, setQuantity] = useState(0)
  const { addCoffeeToCart } = useCart()

  function handleIncrease() {
    setQuantity((state) => state + 1)
  }

  function handleDecrease() {
    setQuantity((state) => state - 1)
  }

  const handleAddToCart = () => {
    const coffeeItem = {
      ...coffee,
      quantity
    }
    addCoffeeToCart(coffeeItem)
  }

  const formattedPrice = formatPrice(coffee.price)

  return (
    <S.Wrapper>
      <img src={`/coffees/${coffee.photo}`} alt="" />
      <S.CardTags>
        {coffee.tags.map((tag, index) => (
          <span key={index}>{tag}</span>
        ))}
      </S.CardTags>

      <S.CardContent>
        <h2>{coffee.name}</h2>
        <p>{coffee.description}</p>
      </S.CardContent>

      <S.CardFooter>
        <S.Price>
          <span>R$</span>
          <h3>{formattedPrice}</h3>
        </S.Price>

        <S.CartContainer>
          <S.QuantityInput>
            <button onClick={handleDecrease} disabled={quantity < 1}>
              <Minus size={14} />
            </button>
            <input type="number" readOnly value={quantity} />
            <button onClick={handleIncrease}>
              <Plus size={14} />
            </button>
          </S.QuantityInput>

          <S.CartButton onClick={handleAddToCart}>
            <ShoppingCart weight="fill" size={22} />
          </S.CartButton>
        </S.CartContainer>
      </S.CardFooter>
    </S.Wrapper>
  )
}
