import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.card};
    border-radius: ${theme.border.radius};
    border-top-right-radius: 3.6rem;
    border-bottom-left-radius: 3.6rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    text-align: center;

    > img {
      margin-top: -2rem;
      max-width: 12rem;
    }
  `}
`

export const CardTags = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 0.4rem;
  margin-top: 1.2rem;
  margin-bottom: 1.6rem;

  span {
    ${({ theme }) => css`
      background: ${theme.colors.yellowLight};
      color: ${theme.colors.yellowDark};
      padding: 0.4rem 0.8rem;
      border-radius: 10rem;
      text-transform: uppercase;
      font-weight: 700;
      font-size: ${theme.font.sizes.size10};
    `}
  }
`

export const CardContent = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: 0.8rem;
    padding: 0 2rem;

    h2 {
      font-family: 'Baloo 2';
      font-size: ${theme.font.sizes.size20};
    }

    p {
      font-size: ${theme.font.sizes.size14};
      color: ${theme.colors.label};
      line-height: 1.6;
    }
  `}
`

export const CardFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 3.8rem;
  padding: 0 2rem;
  margin-top: 3.2rem;
  margin-bottom: 2rem;
`

export const Price = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: baseline;
    font-family: 'Baloo 2';
    font-size: ${theme.font.sizes.size14};
    color: ${theme.colors.text};

    span {
      display: block;
      font-size: ${theme.font.sizes.size14};
      margin-right: 0.4rem;
    }

    h3 {
      font-size: ${theme.font.sizes.size24};
    }
  `}
`

export const CartContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 0.8rem;
`

export const QuantityInput = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 0.4rem;
    background: ${theme.colors.button};
    border-radius: ${theme.border.radius};
    height: 3.8rem;
    padding: 0.8rem;
    width: 7.2rem;

    input {
      color: ${theme.colors.title};
      background: transparent;
      text-align: center;
      border: none;
      width: 100%;

      &:focus {
        outline: none;
      }
    }

    button {
      outline: none;
      background: transparent;
      display: flex;

      svg {
        color: ${theme.colors.purple};
      }
    }
  `}
`

export const CartButton = styled.button`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    background: ${theme.colors.purpleDark};
    color: ${theme.colors.white};
    border-radius: ${theme.border.radius};
    height: 3.8rem;
    width: 3.8rem;
    transition: all 0.2s;

    &:hover {
      background: ${theme.colors.purple};
    }
  `}
`
