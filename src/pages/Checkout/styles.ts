import styled from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  display: flex;
  gap: 3.2rem;
  margin: 4rem auto;
  width: 100%;

  ${media.lessThan('large')`
    flex-direction: column;
  `}
`
