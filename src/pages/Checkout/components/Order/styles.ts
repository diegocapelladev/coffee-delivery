import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  width: 44.8rem;

  ${media.lessThan('large')`
    width: 100%;
  `}
`
export const OrderContainer = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.card};
    border-radius: ${theme.border.radius};
    border-top-right-radius: 3.6rem;
    border-bottom-left-radius: 3.6rem;
    padding: 4rem;
    margin-top: 1.6rem;
    display: flex;
    flex-direction: column;
    width: 100%;

    ${media.lessThan('medium')`
      padding: 2rem;
    `}
  `}
`
export const CoffeeCard = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 2.4rem;
  margin-bottom: 2.4rem;

  ${({ theme }) => css`
    border-bottom: 1px solid ${theme.colors.button};
  `}

  > div {
    display: flex;
    align-items: center;
    gap: 2rem;

    img {
      max-width: 6.4rem;
      max-height: 6.4rem;
    }
  }

  > p {
    text-align: center;
  }

`

export const CardAction = styled.div`
  display: flex;
  align-items: center;
  gap: 0.8rem;
  margin-top: 0.8rem;
`
export const QuantityInput = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 0.4rem;
    background: ${theme.colors.button};
    border-radius: ${theme.border.radius};
    height: 3.2rem;
    padding: 0.8rem;
    width: 7.2rem;

    input {
      color: ${theme.colors.title};
      background: transparent;
      text-align: center;
      border: none;
      width: 100%;

      &:focus {
        outline: none;
      }
    }

    button {
      outline: none;
      background: transparent;
      display: flex;

      svg {
        color: ${theme.colors.purple};
      }
    }
  `}
`

export const RemoveItem = styled.button`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    gap: 0.4rem;
    padding: 0 0.8rem;
    border-radius: ${theme.border.radius};
    background: ${theme.colors.button};
    border: 1px solid ${theme.colors.button};
    height: 3.2rem;
    transition: all 0.2s;

    &:hover {
      border: 1px solid ${theme.colors.purple};
      background: ${theme.colors.purpleLight};
    }

    svg {
      color: ${theme.colors.purple};
    }
  `}
`
export const Price = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.text};
    font-size: ${theme.font.sizes.size16};
    font-weight: 700;
  `}
`
export const TotalPayment = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 1.2rem;
`
export const PaymentItems = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  > p {
    ${({ theme }) => css`
      color: ${theme.colors.text};
      font-size: ${theme.font.sizes.size14};
    `}
  }

  &:last-child {
    > p {
      ${({ theme }) => css`
        color: ${theme.colors.subtitle};
        font-size: ${theme.font.sizes.size20};
        font-weight: bold;
      `}
    }
  }
`

export const ConfirmOrderButton = styled.button`
  ${({ theme }) => css`
    margin-top: 2.4rem;
    height: 4.6rem;
    background: ${theme.colors.yellow};
    color: ${theme.colors.white};
    font-size: ${theme.font.sizes.size14};
    font-weight: 700;
    text-transform: uppercase;
    border-radius: ${theme.border.radius};
    transition: all 0.2s;

    &:hover {
      background: ${theme.colors.yellowDark};
    }

    &:disabled {
      cursor: not-allowed;
      background: ${theme.colors.button};
      color: ${theme.colors.label};
    }
  `}
`
