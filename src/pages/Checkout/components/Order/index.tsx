import { Minus, Plus, Trash } from 'phosphor-react'

import { Heading } from '../../../../components/Heading'
import { useCart } from '../../../../context/CartContext'
import { formatPrice } from '../../../../utils/formatPrice'
import * as S from './styles'

const DeliveryPrice = 3.5

export const Order = () => {
  const {
    cartItems,
    removeCartItem,
    changeCartItemQuantity,
    cartQuantity,
    cartItemsTotal
  } = useCart()

  const cartTotal = DeliveryPrice + cartItemsTotal

  const formattedDelivery = formatPrice(DeliveryPrice)
  const formattedSubTotal = formatPrice(cartItemsTotal)
  const formattedTotal = formatPrice(cartTotal)

  function handleIncrease(id: number) {
    changeCartItemQuantity(id, 'increase')
  }

  function handleDecrease(id: number) {
    changeCartItemQuantity(id, 'decrease')
  }

  function handleRemoveItem(cartItemId: number) {
    removeCartItem(cartItemId)
  }

  const CartItem = cartItems.map((coffee) => {
    const totalCoffee = coffee.price * coffee.quantity
    const formattedPrice = formatPrice(totalCoffee)

    return (
      <S.CoffeeCard key={coffee.id}>
        <div>
          <img src={`/coffees/${coffee.photo}`} alt={coffee.name} />

          <div>
            <p>{coffee.name}</p>
            <S.CardAction>
              <S.QuantityInput>
                <button
                  onClick={() => handleDecrease(coffee.id)}
                  disabled={coffee.quantity <= 1}
                >
                  <Minus size={14} />
                </button>
                <input type="number" readOnly value={coffee.quantity} />
                <button onClick={() => handleIncrease(coffee.id)}>
                  <Plus size={14} />
                </button>
              </S.QuantityInput>

              <S.RemoveItem onClick={() => handleRemoveItem(coffee.id)}>
                <Trash size={16} />
                Remover
              </S.RemoveItem>
            </S.CardAction>
          </div>
        </div>

        <S.Price>R$ {formattedPrice}</S.Price>
      </S.CoffeeCard>
    )
  })

  return (
    <S.Wrapper>
      <Heading color="title" size="small">
        Cafés selecionados
      </Heading>
      <S.OrderContainer>
        {CartItem}

        <S.TotalPayment>
          <S.PaymentItems>
            <p>Total de itens</p>
            <p>R$ {formattedSubTotal}</p>
          </S.PaymentItems>

          <S.PaymentItems>
            <p>Entrega</p>
            <p>R$ {cartItems.length > 0 ? formattedDelivery : '0,00'}</p>
          </S.PaymentItems>

          <S.PaymentItems>
            <p>Total</p>
            <p>R$ {cartItems.length > 0 ? formattedTotal : '0,00'}</p>
          </S.PaymentItems>
        </S.TotalPayment>

        <S.ConfirmOrderButton type="submit" disabled={cartQuantity < 0}>
          Confirmar pedido
        </S.ConfirmOrderButton>
      </S.OrderContainer>
    </S.Wrapper>
  )
}
