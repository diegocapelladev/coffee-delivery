import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  flex: 1;
`
export const ShippingContainer = styled.div`
  ${({ theme }) => css`
    background: ${theme.colors.card};
    border-radius: ${theme.border.radius};
    padding: 4rem;
    margin-top: 1.6rem;
  `}

  ${media.lessThan('medium')`
    padding: 2rem;
  `}
`
export const Title = styled.div`
  ${({ theme }) => css`
    display: flex;
    gap: 0.8rem;
    line-height: 1.6;

    svg {
      color: ${theme.colors.yellowDark};;
    }

    > div {
      h2 {
        color: ${theme.colors.subtitle};
        font-size: ${theme.font.sizes.size16};
        font-weight: 400;
      }

      p {
        color: ${theme.colors.text};
        font-size: ${theme.font.sizes.size14};
      }
    }
  `}
`

export const FormContainer = styled.div`
  margin-top: 3.2rem;
  display: flex;
  flex-direction: column;
  row-gap: 1.6rem;

  > input {
    ${media.lessThan('medium')`
      width: 100%;
    `}
  }
`

export const InputBase = styled.input`
  ${({ theme }) => css`
    height: 4.2rem;
    border: none;
    background: #EEEDED;
    border: 1px solid #E6E5E5;
    color: ${theme.colors.text};
    font-size: ${theme.font.sizes.size14};
    border-radius: 0.4rem;
    padding: 1.2rem;

    &:focus {
      border: 1px solid ${theme.colors.yellowDark};
    }

    &::placeholder {
      color: ${theme.colors.label};
    }
  `}
`

export const CEP = styled(InputBase)`
  width: 20rem;
`
export const Rua = styled(InputBase)`
  width: 100%;
`
export const Numero = styled(InputBase)`
  width: 20rem;
`

export const Complemento = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    height: 4.2rem;
    border: 1px solid #E6E5E5;
    background: #EEEDED;
    border-radius: 0.4rem;
    padding: 1.2rem;

    &:focus-within {
      border: 1px solid #c47f17;
    }

    input {
      flex: 1;
      border: none;
      background: transparent;
      color: ${theme.colors.text};
      font-size: ${theme.font.sizes.size14};

      &::placeholder {
        color: ${theme.colors.label};
      }
    }


    p {
      font-style: italic;
      font-size: ${theme.font.sizes.size12};
      color: ${theme.colors.label};
    }
  `}
`

export const Bairro = styled(InputBase)`
  width: 20rem;
`

export const Cidade = styled(InputBase)`
  flex: 1;
`

export const UF = styled(InputBase)`
  width: 6rem;
`
export const LineForm = styled.div`
  display: flex;
  column-gap: 1.2rem;

  ${media.lessThan('medium')`
    row-gap: 1.6rem;
    flex-direction: column;
  `}
`
// Payment

export const PaymentContainer = styled.div`
  ${({ theme }) => css`
    margin-top: 1.2rem;
    background: ${theme.colors.card};
    border-radius: ${theme.border.radius};
    padding: 4rem;

    svg {
      color: ${theme.colors.purple};;
    }

    ${media.lessThan('medium')`
      padding: 2rem;
    `}
  `}
`
export const PaymentButtons = styled.div`
  margin-top: 3.2rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 1.2rem;
  height: 5rem;

  ${media.lessThan('medium')`
    flex-direction: column;
    height: 15rem;
    > label {
      display: block;
      height: 100%;
      width: 100%;
    }
  `}

  input {
    visibility: hidden;
    appearance: none;
    display: none;
  }

  ${({ theme }) => css`
    input:checked + label {
      background: ${theme.colors.purpleLight};
      border: 1px solid ${theme.colors.purple};
    }
  `}

  label {
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    gap: 1.2rem;
    padding: 0 1rem;
    text-transform: uppercase;
    border: 1px solid transparent;
    cursor: pointer;
    transition: all 0.2s;

    ${({ theme }) => css`
      color: ${theme.colors.text};
      background: ${theme.colors.button};
      border-radius: ${theme.border.radius};
      font-size: ${theme.font.sizes.size12};

      &:hover {
        background: ${theme.colors.purpleLight};
        border: 1px solid ${theme.colors.purple};
      }
    `}
  }
`
