import {
  Bank,
  CreditCard,
  CurrencyDollar,
  MapPinLine,
  Money
} from 'phosphor-react'
import { useFormContext } from 'react-hook-form'

import { Heading } from '../../../../components/Heading'
import * as S from './styles'

export const ShippingForm = () => {
  const { register } = useFormContext()
  return (
    <S.Wrapper>
      <Heading size="small" color="title">
        Complete seu pedido
      </Heading>

      <S.ShippingContainer>
        <S.Title>
          <MapPinLine size={22} />
          <div>
            <h2>Endereço de Entrega</h2>
            <p>Informe o endereço onde deseja receber seu pedido</p>
          </div>
        </S.Title>
        <S.FormContainer>
          <S.CEP type="text" placeholder="CEP" {...register('cep')} />
          <S.Rua type="text" placeholder="Rua" {...register('street')} />

          <S.LineForm>
            <S.Numero
              type="text"
              placeholder="Numero"
              {...register('number')}
            />
            <S.Complemento>
              <input
                type="text"
                placeholder="Complemento"
                {...register('complement')}
              />
              <p>Opcional</p>
            </S.Complemento>
          </S.LineForm>

          <S.LineForm>
            <S.Bairro
              type="text"
              placeholder="Bairro"
              {...register('district')}
            />
            <S.Cidade type="text" placeholder="Cidade" {...register('city')} />
            <S.UF type="text" placeholder="UF" {...register('uf')} />
          </S.LineForm>
        </S.FormContainer>
      </S.ShippingContainer>

      <S.PaymentContainer>
        <S.Title>
          <CurrencyDollar size={22} />
          <div>
            <h2>Pagamento</h2>
            <p>
              O pagamento é feito na entrega. Escolha a forma que deseja pagar
            </p>
          </div>
        </S.Title>

        <S.PaymentButtons>
          <input type="radio" name="paymentMethod" id="credit" />
          <label htmlFor="credit">
            <CreditCard size={16} />
            Cartão de crédito
          </label>

          <input type="radio" name="paymentMethod" id="debit" />
          <label htmlFor="debit">
            <Bank size={16} />
            Cartão de débito
          </label>

          <input type="radio" name="paymentMethod" id="money" />
          <label htmlFor="money">
            <Money size={16} />
            Dinheiro
          </label>
        </S.PaymentButtons>
      </S.PaymentContainer>
    </S.Wrapper>
  )
}
