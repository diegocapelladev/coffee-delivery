import { FormProvider, useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import * as zod from 'zod'

import { Container } from '../../components/Container'
import { Order } from './components/Order'
import { ShippingForm } from './components/ShippingForm'
import * as S from './styles'
import { useNavigate } from 'react-router-dom'
import { useCart } from '../../context/CartContext'

const confirmOrderFormValidationSchema = zod.object({
  cep: zod.string().min(1, 'Informe o CEP'),
  street: zod.string().min(1, 'Informe o Rua'),
  number: zod.string().min(1, 'Informe o Número'),
  complement: zod.string(),
  district: zod.string().min(1, 'Informe o Bairro'),
  city: zod.string().min(1, 'Informe a Cidade'),
  uf: zod.string().min(1, 'Informe a UF')
})

export type OrderData = zod.infer<typeof confirmOrderFormValidationSchema>

type ConfirmOrderFormData = OrderData

export const Checkout = () => {
  const confirmOrderForm = useForm<ConfirmOrderFormData>({
    resolver: zodResolver(confirmOrderFormValidationSchema)
  })

  const { handleSubmit } = confirmOrderForm

  const navigate = useNavigate()
  const { clearCart } = useCart()

  function handleConfirmOrder(data: ConfirmOrderFormData) {
    navigate('/shipping', {
      state: data
    })
    clearCart()
  }

  return (
    <Container>
      <FormProvider {...confirmOrderForm}>
        <form onSubmit={handleSubmit(handleConfirmOrder)}>
          <S.Wrapper>
            <ShippingForm />
            <Order />
          </S.Wrapper>
        </form>
      </FormProvider>
    </Container>
  )
}
