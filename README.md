# Projeto Coffee Delivery Ignite da Rocketseat

## Projeto desenvolvido utilizando React, Context API, TypeScript, Styled-components e responsivo

## Exec

- Instale as dependências com: `yarn` ou `npm install`
- Inicie o projeto com: `yarn dev` ou `npm run dev`
- Acesse em: http://localhost:3000

## Projeto Final

<img src="./src/assets/capa/capa-1-coffee-delivery.png" />

---

<img src="./src/assets/capa/capa-2-coffee-delivery.png" />
